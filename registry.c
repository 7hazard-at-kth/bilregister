#include "registry.h"
#include "util.h"
#include <string.h>

registry_node* registry_node_create()
{
    registry_node* node = (registry_node*)malloc(sizeof(registry_node));
    node->prev = NULL;
    node->next = NULL;
    node->val = NULL;
    return node;
}

void registry_node_dispose(registry_node* node)
{
    vehicle_dispose(node->val);
    free(node);
}

void read_delim(FILE* file, char* buf, size_t max, char delim, char suffix)
{
    size_t i = 0;
    char c;
    while ((c = getc(file)) != delim && c != EOF && i < max)
    {
        buf[i] = c;
        i++;
    }

    buf[i] = suffix;
}

registry* registry_create(const char* filename)
{
    registry* reg = (registry*)malloc(sizeof(registry));

    reg->head = NULL;
    reg->tail = NULL;
    reg->filename = malloc(strlen(filename)+1);
    strcpy(reg->filename, filename);

    FILE* file = fopen(filename, "rb");
    if(file == NULL)
    {
        printf("Could not read from file, perhaps it is a new file\n");
        printf("Continue with a fresh registry? [y/n]: ");
        char ok;
        get_input(" %c", &ok);
        while (ok != 'n')
        {
            if(ok == 'y')
                return reg;
            else
            {
                printf("Unknown input, y/n: ");
            }
        }

        return NULL;
    }
    else
    {
        char buf[1024];
        while (!feof(file))
        {   
            // regnr
            read_delim(file, buf, sizeof(buf), '\x0', '\0');

            // because feof check in while is not proper
            // we check after we've read a bit more
            if(feof(file))
                break;

            // we allocate a vehicle
            // when we're sure we're not in the eof
            vehicle* veh = vehicle_create();
            vehicle_set_regnr(veh, buf);
            
            // brand
            read_delim(file, buf, sizeof(buf), '\x0', '\0');
            vehicle_set_brand(veh, buf);
            
            // color
            read_delim(file, buf, sizeof(buf), '\x0', '\0');
            vehicle_set_color(veh, buf);
            
            // year
            read_delim(file, buf, sizeof(buf), '\x0', '\0');
            vehicle_set_year(veh, buf);
            
            registry_add(reg, veh);
        }

        fclose(file);
    }

    return reg;
}

bool registry_save(registry* reg)
{
    FILE* file = fopen(reg->filename, "wb");
    if(file == NULL)
    {
        return false;
    }

    registry_node* cur = reg->head;
    while(cur != NULL)
    {
        vehicle* veh = cur->val;
        
        fputs(veh->regnr, file); fwrite("\x0", 1, 1, file);
        fputs(veh->brand, file); fwrite("\x0", 1, 1, file);
        fputs(veh->color, file); fwrite("\x0", 1, 1, file);
        fputs(veh->year, file); fwrite("\x0", 1, 1, file);

        cur = cur->next;
    }

    fclose(file);

    return true;
}

bool registry_add(registry* reg, vehicle* veh)
{
    registry_node* new_node = registry_node_create();
    new_node->val = veh;
    new_node->prev = reg->tail;

    // If it's null, the registry is empty
    // The tail is also unset then
    if(reg->head == NULL)
    {
        reg->head = new_node;
        reg->tail = new_node;
    }
    else
    {
        reg->tail->next = new_node; // Set the tail's next to the new node
        new_node->prev = reg->tail; // Set the new node's previous to the current tail
        reg->tail = new_node; // Set the tail to the new node
    }

    return true;
}

vehicle* registry_find(registry* reg, char* regnr)
{
    registry_node* cur = reg->head;

    while(cur != NULL)
    {
        // match the regnr
        if(strcmp(regnr, cur->val->regnr) == 0)
        {
            return cur->val;
        }

        // continue
        cur = cur->next;
    }
    
    return NULL;
}

bool registry_remove(registry* reg, char* regnr)
{
    // find the vehicle in registry
    registry_node* cur = reg->head;

    while(cur != NULL)
    {
        // match the regnr
        if(strcmp(regnr, cur->val->regnr) == 0)
        {
            break;
        }

        // continue
        cur = cur->next;
    }

    // if not found
    if(cur == NULL)
    {
        return false;
    }

    // set the current's prev's next to the current's next
    if(cur->prev != NULL)
    {
        cur->prev->next = cur->next;
    }
    // set the current's next's prev to the cur's prev
    if(cur->next != NULL)
    {
        cur->next->prev = cur->prev;
    }

    // if the node was in the beginning
    if(cur == reg->head)
    {
        // set the head to the current's next
        reg->head = cur->next;
    }

    // if the node was in the end
    if(cur == reg->tail)
    {
        // set the tail to the current's prev
        reg->tail = cur->prev;
    }

    registry_node_dispose(cur);

    return true;
}