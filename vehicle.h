#ifndef _VEHICLE_H_
#define _VEHICLE_H_

#include <stdbool.h>

#define VEHICLE_REGNR_SIZE 7

typedef struct vehicle
{
    const char* regnr;
    const char* brand;
    const char* color;
    const char* year;
} vehicle;

// allocates a new vehicle
vehicle* vehicle_create();
// deallocates the vehicle
void vehicle_dispose(vehicle* veh);
// copies veh info from src to dest
void vehicle_copy(vehicle* dest, vehicle* src);

/// setters
// sets the regnr
void vehicle_set_regnr(vehicle* veh, char* regnr);
void vehicle_set_brand(vehicle* veh, const char* brand);
void vehicle_set_color(vehicle* veh, const char* color);
void vehicle_set_year(vehicle* veh, const char* year);

/// util
void vehicle_print(vehicle* veh);

// Returns false if invalid
bool vehicle_validate_year(char* year);
// Returns false if invalid
bool vehicle_validate_regnr(char* regnr);

#endif // _VEHICLE_H_
