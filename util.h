#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

static int get_input(char const* const format, ...)
{
    va_list args;
    va_start(args, format);
    int res = vscanf(format, args);
    va_end(args);
    scanf("%*[^\n]");
    // scanf("%*c");

    return res;
}

static void capitalize(char* str, size_t start, size_t len)
{
    for (size_t i = start; i < len; i++)
    {
        str[i] = toupper(str[i]);
    }
    
}

#endif // _UTIL_H_
