#include "vehicle.h"
#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>

vehicle* vehicle_create()
{
    vehicle* veh = (vehicle*)malloc(sizeof(vehicle));
    veh->regnr = NULL;
    veh->brand = NULL;
    veh->color = NULL,
    veh->year = NULL;
    return veh;
}

void vehicle_dispose(vehicle* veh)
{
    free(veh->regnr);
    free(veh->brand);
    free(veh->color);
    free(veh->year);
    free(veh);
}

void vehicle_copy(vehicle* dest, vehicle* src)
{
    vehicle_set_regnr(dest, src->regnr);
    vehicle_set_brand(dest, src->brand);
    vehicle_set_color(dest, src->color);
    vehicle_set_year(dest, src->year);
}

void vehicle_set_regnr(vehicle* veh, char* regnr)
{
    if(veh->regnr != NULL)
    {
        free(veh->regnr);
    }

    char* regnr_m = (char*)malloc(VEHICLE_REGNR_SIZE);
    strcpy(regnr_m, regnr);
    veh->regnr = regnr_m;
}

void vehicle_set_brand(vehicle* veh, const char* brand)
{
    if(veh->brand != NULL)
    {
        free(veh->brand);
    }

    char* m = malloc(strlen(brand)+1);
    strcpy(m, brand);
    m[0] = toupper(m[0]); // capitalize first letter
    veh->brand = m;
}

void vehicle_set_color(vehicle* veh, const char* color)
{
    if(veh->color != NULL)
    {
        free(veh->color);
    }

    char* m = malloc(strlen(color)+1);
    strcpy(m, color);
    m[0] = toupper(m[0]); // capitalize first letter
    veh->color = m;
}

void vehicle_set_year(vehicle* veh, const char* year)
{
    if(veh->year != NULL)
    {
        free(veh->year);
    }

    char* m = malloc(5);
    strcpy(m, year);
    veh->year = m;
}

void vehicle_print(vehicle* veh)
{
    static const char* veh_info = "\n\
Registration: %s\n\
Brand: %s\n\
Color: %s\n\
Year: %s\n\
";

    printf(
        veh_info,
        veh->regnr,
        veh->brand,
        veh->color,
        veh->year
    );
}

// Temp stack string
// char* vehicle_info()
// {
//     char info[512];
//     return 
// }

bool vehicle_validate_year(char* year)
{
    for (size_t i = 0; i < 4; i++)
    {
        if(!isdigit(year[i]))
            return false;
    }
    
    return true;
}

bool vehicle_validate_regnr(char* regnr)
{
    // Check if first 3 characters are letters
    for (int i = 0; i < 3; i++)
    {
        if(!isalpha(regnr[i]))
            return false;
        regnr[i] = toupper(regnr[i]);
    }

    // Check if the next 3 letters are numbers
    for (int i = 3; i < 6; i++)
    {
        if(!isdigit(regnr[i]))
            return false;
    }

    return true;
}
