#ifndef _REGISTRY_H_
#define _REGISTRY_H_

#include "vehicle.h"
#include <stdbool.h>
#include <malloc.h>
#include <stdio.h>

typedef struct registry_node
{
    vehicle *val;
    struct registry_node *next, *prev;
} registry_node;

typedef struct registry
{
    registry_node *head, *tail;
    const char* filename;
} registry;

registry* registry_create(const char* filename);
// saves current registry to file, returns false if fails
bool registry_save(registry* reg);
// save should be true if the addition should be written to file
bool registry_add(registry* reg, vehicle* veh);
// returns NULL if not found
vehicle* registry_find(registry* reg, char* regnr);
bool registry_remove(registry* reg, char* regnr);

#endif // _REGISTRY_H_
