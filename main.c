#include "registry.h"
#include "util.h"
#include <stdbool.h>
#include <stdio.h>

int main()
{
    static const char* info = "\n\
Select an action:\n\
\n\
1. Find vehicle\n\
2. Add vehicle\n\
3. Edit vehicle\n\
4. Remove vehicle\n\
5. List all vehicles\n\
0. Exit\n\
";

    printf("Vehicle registry (bilregister)\n");

    // Select file to use
    printf("Select filename to read and save registry to: ");
    char filename[256];
    get_input(" %255s", filename);

    // Registry, reads from filename
    registry* reg = registry_create(filename);
    if(reg == NULL)
    {
        // printf("Could not open registry from %s (enter to continue)\n", filename);
        // getchar();
        // getchar();
        return -1;
    }

    // Selection loop
    while (true)
    {
        printf("%s", info);

        int selection = 0;
        get_input(" %i", &selection);

        switch (selection)
        {
        case 0: // Exit
        {
            printf("Exiting...");
            return 0;
        }

        case 1: // Find
        {
            printf("Enter registration number to find: ");
            char regnr[VEHICLE_REGNR_SIZE] = "------";
            get_input(" %6s", regnr);

            if(!vehicle_validate_regnr(regnr))
            {
                printf("Registry number is not in the valid format\n");
                break;
            }

            vehicle* veh = registry_find(reg, regnr);
            if(veh == NULL)
            {
                printf("Vehicle not found\n");
                break;
            }
            else {
                vehicle_print(veh);
            }

            break;
        }


        case 2: // Add
        {
            // Format ABC123, 6 chars
            printf("Enter registration number in format ABC123: ");
            char regnr[VEHICLE_REGNR_SIZE] = "------";
            get_input(" %6s", regnr);
            
            if(!vehicle_validate_regnr(regnr))
            {
                printf("Registry number is not in the valid format\n");
                break;
            }

            vehicle* veh = registry_find(reg, regnr);
            // check if vehicle already exists
            if(veh == NULL)
            {
                veh = vehicle_create();
            }
            else
            {
                char edit_current;
                printf("Vehicle already exists, aborting\n");
                break;
            }

            // set info values
            vehicle_set_regnr(veh, regnr);

            // brand
            {
                printf("Enter the brand of the vehicle (max 128 characters): ");
                char buf[128];
                get_input(" %128s", buf);

                vehicle_set_brand(veh, buf);
            }

            // color
            {
                printf("Enter the color of the vehicle (max 24 characters): ");
                char buf[24];
                get_input(" %24s", buf);

                vehicle_set_color(veh, buf);
            }

            // year
            {
                printf("Enter the year of the vehicle: ");
                char buf[5];
                get_input(" %4s", buf);
                
                while(!vehicle_validate_year(buf))
                {
                    printf("Please enter valid 4-digit year: ");
                    get_input(" %4s", buf);
                }

                vehicle_set_year(veh, buf);
            }

            // Output summary of new vehicle
            vehicle_print(veh);
            
            // prompt confirmation
            printf("\nConfirm new vehicle? [y/n]: ");
            char confirm = '-';
            while (true)
            {
                get_input(" %c", &confirm);

                if(confirm == 'y' || confirm == 'n')
                {
                    break;
                }
                else
                {
                    printf("Unknown input, 'y' for yes or 'n' for no\nConfirm new vehicle?");
                    continue;
                }
            }
            
            // stop if confirm is no
            if(confirm == 'n')
            {
                printf("Aborted\n");
                break;
            }

            // finish adding the vehicle
            registry_add(reg, veh);
            printf("Vehicle added\n");

            if(!registry_save(reg))
            {
                printf("Could save registry to file\n");
            }

            break;
        }

        case 3: // Edit
        {
            printf("Enter registration number to edit: ");
            char regnr[VEHICLE_REGNR_SIZE] = "------";
            get_input(" %6s", regnr);

            if(!vehicle_validate_regnr(regnr))
            {
                printf("Registry number is not in the valid format\n");
                break;
            }

            vehicle* veh = registry_find(reg, regnr);
            if(veh == NULL)
            {
                printf("Could not find vehicle, aborting\n");
                break;
            }

            // print current info
            printf("\nCurrent vehicle info:");
            vehicle_print(veh);

            // Set new info
            vehicle* new_veh = vehicle_create();
            printf("\nEnter registration number in format ABC123: ");
            get_input(" %6s", regnr);
            
            if(!vehicle_validate_regnr(regnr))
            {
                printf("Registry number is not in the valid format\n");
                break;
            }

            vehicle_set_regnr(new_veh, regnr);

            // brand
            {
                printf("Enter the brand of the vehicle (max 128 characters): ");
                char buf[128];
                get_input(" %128s", buf);

                vehicle_set_brand(new_veh, buf);
            }

            // color
            {
                printf("Enter the color of the vehicle (max 24 characters): ");
                char buf[24];
                get_input(" %24s", buf);

                vehicle_set_color(new_veh, buf);
            }

            // year
            {
                printf("Enter the year of the vehicle: ");
                char buf[5];
                get_input(" %4s", buf);
                
                while(!vehicle_validate_year(buf))
                {
                    printf("Please enter valid 4-digit year: ");
                    get_input(" %4s", buf);
                }

                vehicle_set_year(new_veh, buf);
            }

            printf("\nNew vehicle info:");
            vehicle_print(new_veh);

            // prompt confirmation
            printf("\nAre you sure you want to edit? [y/n]: ");
            char confirm = '-';
            while (true)
            {
                get_input(" %c", &confirm);

                if(confirm == 'y' || confirm == 'n')
                {
                    break;
                }
                else
                {
                    printf("Unknown input, 'y' for yes or 'n' for no\nConfirm new vehicle?");
                    continue;
                }
            }
            
            // stop if confirm is no
            if(confirm == 'n')
            {
                printf("Aborted\n");
                break;
            }

            vehicle_copy(veh, new_veh);
            vehicle_dispose(new_veh);

            printf("Vehicle edited\n");

            if(!registry_save(reg))
                printf("Could not save registry to file\n");

            break;
        }

        case 4: // Remove
        {
            printf("Enter registration number to remove: ");
            char regnr[VEHICLE_REGNR_SIZE] = "------";
            get_input(" %6s", regnr);

            if(!vehicle_validate_regnr(regnr))
            {
                printf("Registry number is not in the valid format\n");
                break;
            }

            vehicle* veh = registry_find(reg, regnr);

            if(veh == NULL)
            {
                printf("Vehicle does not exist\n");
                break;
            }

            vehicle_print(veh);

            // prompt confirmation
            printf("\nAre you sure you want to remove? [y/n]: ");
            char confirm = '-';
            while (true)
            {
                get_input(" %c", &confirm);

                if(confirm == 'y' || confirm == 'n')
                {
                    break;
                }
                else
                {
                    printf("Unknown input, 'y' for yes or 'n' for no\nConfirm removal?");
                    continue;
                }
            }
            
            // stop if confirm is no
            if(confirm == 'n')
            {
                printf("Aborted\n");
                break;
            }

            registry_remove(reg, regnr);

            printf("Removed vehicle");

            if(!registry_save(reg))
                printf("Could not save registry to file\n");

            break;
        }
        
        case 5: // List all vehicles
        {
            registry_node* cur = reg->head;

            if(cur == NULL)
            {
                printf("There are no vehicles in the registry\n");
                break;
            }

            while(cur != NULL)
            {
                vehicle_print(cur->val);
                cur = cur->next;
            }

            break;
        }
        
        default:
        {
            printf("Unknown selection %i\n", selection);
            break;
        }
        }
    }

    return 0;
}
